import * as THREE from 'three';
import 'regenerator-runtime/runtime'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import Viewer from './viewer';

export default class App {
  constructor(container, number, wolfClaimed, merchBox) {
    this.number = number;
    this.wolfClaimed = wolfClaimed
    this.merchBox = merchBox
    this.viewer = new Viewer(container, this);
    this.loaderGLTF = new GLTFLoader();
    this.initHorizont();
    this.envTexture = new THREE.TextureLoader().load( 'assets/env.jpg', () => {
      this.envTexture.wrapS = this.envTexture.wrapT = THREE.RepeatWrapping;
      this.envTexture.mapping = THREE.EquirectangularReflectionMapping
      this.envTexture.encoding = THREE.sRGBEncoding;
    });
  }
  loadingManager() {
    setTimeout(() => {
      this.loading  = document.getElementById('loading')
      this.loading.classList.add('hidden')
    }, 1000)
  }

  async loaderCard() {
    let xhr = new XMLHttpRequest();
    let progress = 0;

    const bar = document.getElementById('progress-bar');
    const onProgress = (xhr) =>  {
        // progress = xhr.loaded / xhr.total;
        progress = xhr.loaded / 8689532;
        bar.style.transform =  `scaleX(${(0.01 + progress)})`;
    }
    const gltf = await new GLTFLoader().loadAsync( './assets/bad-wolfes_6.glb', (xhr)=> {onProgress(xhr)} );
    this.card = gltf.scene;
    this.card.position.set(0, -1.55, 0)
    this.card.traverse(item => { 
      if(item.name === 'CardOutline005') {
        item.material.envMap = this.envTexture;
        item.material.metalness = 0.6;
        item.material.roughness = 0;
        item.material.color.setRGB(4, 4, 4)
        item.material.envMapIntensity = 0.8;
        item.needsUpdate = true;
        item.material.normalScale.set(3, -3)
      }
      if(item.name === 'Plane004') {
        this.cavasNumber = document.getElementById('canvasNumber')
        const ctx = this.cavasNumber.getContext("2d");
        ctx.fillStyle =  "rgba(0, 0, 0, 1)";
        ctx.font = "13px Finger Paint";
        ctx.textAlign = "center";
        ctx.fillText(`${this.number} / 500`, 44, 45)
        item.material = new THREE.MeshBasicMaterial({  
          map : new THREE.CanvasTexture(  this.cavasNumber, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping),
          side: THREE.DoubleSide, transparent: true});
        item.material.map.flipY = false;
        item.material.transparent = true;
        item.material.needsUpdate = true;
      }


      if(item.name === 'Plane001') {
        if(!this.wolfClaimed) {
          item.visible = false;
          return;
        }
        item.position.set(0.601631875038147, 1.4201896543502812, 0.16960664093494415) 
        item.scale.x *= -1
        item.material.metalness = 0;
        item.material.roughness = 0.5;
        item.material = new THREE.MeshBasicMaterial({  
            map: new THREE.TextureLoader().load( 'assets/marker.png'),
            side: THREE.DoubleSide, transparent: true});
      }

      if(item.name === 'Plane'){
        if(!this.merchBox) {
          item.visible = false;
          return;
        }
        item.position.set(0.6064795255661011, 1.3121151504516595, 0.17023982107639313) 
        console.log(item)
        item.scale.x *= -1
        item.material.metalness = 0;
        item.material.roughness = 0.5;
        item.material = new THREE.MeshBasicMaterial({  
            map: new THREE.TextureLoader().load( 'assets/marker.png'),
            side: THREE.DoubleSide, transparent: true});
      }


    })
    this.viewer.scene.add(this.card);
    this.loadingManager()
  }
  initHorizont() {
    this.horizont = new THREE.Mesh( 
      new THREE.PlaneGeometry(10, 10),
      new THREE.MeshBasicMaterial( {color: 0xffffff, map:  new THREE.TextureLoader().load( './assets/bg.jpg' ) ,side: THREE.DoubleSide} ) 
  );
    this.horizont.position.z += -12;
    this.horizont.position.y += 0;
    this.horizont.position.x += 0;
    this.viewer.camera.add(this.horizont);
    this.loaderCard();
  }

}
