import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
  
  export default class Viewer {
    constructor(container, app) {
        this.app = app;
        this.container = container;
        this.init();
    }
  
    init() {
  
      // CAMERA
      this.camera = new THREE.PerspectiveCamera(
        45,
        this.container.offsetWidth / this.container.offsetHeight,
        1,
        2000,
      );
      this.camera.position.set(0, 0, 3);

      // SCENE
      this.scene = new THREE.Scene();
      window.scene = this.scene;
      this.scene.add(this.camera);
  
      // RENDERER
      this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
      this.renderer.setPixelRatio(2);
      this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);

      // this.renderer.toneMapping = THREE.ReinhardToneMapping;
      // this.renderer.toneMappingExposure = 1;
      // this.renderer.shadowMap.enabled = true;

      // this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
      // this.renderer.localClippingEnabled = true;

      // this.renderer.outputEncoding = THREE.sRGBEncoding;
      // this.renderer.toneMapping = 0;

      // this.renderer.outputEncoding = THREE.sRGBEncoding;
      // this.renderer.shadowMap.enabled = true;
  
      // ADD ON SCENE
      this.container.appendChild(this.renderer.domElement);
  
      // LIGHT
      this.initLight();
  
      // CONTROLS
      this.initControls();
  
      // RESIZE EVENT
      window.addEventListener('resize', this.onWindowResize.bind(this));
  
      setTimeout(() => {
        this.onWindowResize();
      }, 0);
  
      this.animate();
    }
  
    initControls() {
      this.controls = new OrbitControls(this.camera, this.renderer.domElement);
      this.controls.enablePan = false;
      this.controls.enableZoom = false;


      this.controls.target = new THREE.Vector3(0, 0, 0);
      this.controls.update();

    }
  
    initLight() {
      this.hemisphereLight = new THREE.HemisphereLight(0xffffff, 0x000000, 1.5);
      this.hemisphereLight.name = 'HemisphereLight';
      this.hemisphereLight.position.set(0, 10, 0);
      this.scene.add(this.hemisphereLight);
    }
  
    onWindowResize() {
      this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
    }
  
    animate() {
      requestAnimationFrame(() => {
        this.animate();
      });
      if(this.app.card)  this.app.card.rotation.y += 0.005;
      this.controls.update();
      this.renderer.render(this.scene, this.camera);
    }
  }
  