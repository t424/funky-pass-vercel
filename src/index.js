import App from './app';
import "./styles/index.scss";

document.addEventListener('DOMContentLoaded', function(){ 
    const loaderBar = document.getElementById('loader-progress-bar');
    const mainFunck = async () => {
        const params = new Proxy(new URLSearchParams(window.location.search), {
            get: (searchParams, prop) => searchParams.get(prop),
        });
        if(params.id <= 500 && params.id > 0) {
            loaderBar.style.display = 'block';
            let number = null;
            let wolfClaimed = null;
            let merchBox = null;
            if(params.id < 10) {
                 number = `00${params.id}`
            }
            if(params.id > 9 && params.id < 100 ) {
                number = `0${params.id}`
            }
            if(params.id > 99) {
                number = `${params.id}`
            }
            if(params.wolfClaimed === "true" ) {
                wolfClaimed = true;
            } else {
                wolfClaimed = false;
            }
            if(params.merchBox === "true" ) {
                merchBox = true;
            } else {
                merchBox = false;
            }
            const numberFunkyPass = document.getElementById('number-funky-pass');
            numberFunkyPass.innerHTML = `${number}/500`
            const container = document.getElementById('root');
            new App(container, number, wolfClaimed, merchBox);
        }
    }
    mainFunck();
});